#include <srrg_system_utils/parse_command_line.h>
#include <srrg_system_utils/shell_colors.h>

#include <srrg_solver/solver_core/instances.h>
#include <srrg_solver/solver_core/internals/linear_solvers/instances.h>
#include <srrg_solver/solver_core/solver.h>
#include <srrg_solver/variables_and_factors/types_2d/se2_prior_error_factor.h>
#include "variables_and_factors/se2_motion_control_prediction_factor_ad.h"

const std::string exe_name = "test_se2_splac_ad";
#define LOG std::cerr << exe_name + "|"

using namespace srrg2_core;
using namespace srrg2_solver;
using namespace srrg2_solver_extras;

const size_t n_meas       = 1000;
const size_t n_iterations = 10;
const float time_step     = 0.01;

Vector3f predictRobotMotion(const Vector2f& control_input_) {
  // bb runge-kutta integration
  const float& linear_velocity  = control_input_.x();
  const float& angular_velocity = control_input_.y();
  const float dx = linear_velocity * time_step * cos(angular_velocity * time_step / 2);
  const float dy = linear_velocity * time_step * sin(angular_velocity * time_step / 2);
  const float dt = angular_velocity * time_step;
  return Vector3f(dx, dy, dt);
}

int main(int argc, char** argv) {
  using StateType                    = VariableSE2RightAD;
  using StatePtrType                 = std::shared_ptr<StateType>;
  using ControlType                  = VariableControl2AD;
  using ControlPtrType               = std::shared_ptr<ControlType>;
  using PredictionFactorType         = SE2MotionControlPredictionFactorAD;
  using PredictionFactorPtrType      = std::shared_ptr<PredictionFactorType>;
  using PriorFactorType              = SE2PriorErrorFactor;
  using PriorFactorPtrType           = std::shared_ptr<PriorFactorType>;
  const Vector3f initial_pose_vector = 10 * Vector3f::Random();
  Isometry2f initial_pose_transform  = geometry2d::v2t(initial_pose_vector);
  const Vector2f control_input       = Vector2f::Ones();
  Vector3f ego_motion                = Vector3f::Zero();
  Isometry2f robot_motion            = Isometry2f::Identity();
  Isometry2f robot_motion_prediction = Isometry2f::Identity();

  // bb create pointers to variables corresponding to poses and controls
  // bb set their initial guess, aka the estimate, and graph id
  StatePtrType pose_0 = StatePtrType(new StateType);
  pose_0->setEstimate(Isometry2f::Identity());
  pose_0->setGraphId(0);
  ControlPtrType control_0 = ControlPtrType(new ControlType);
  control_0->setEstimate(Vector2f::Zero());
  control_0->setGraphId(1);
  // bb create pointer to prediction factor
  PredictionFactorPtrType prediction_factor_0 = PredictionFactorPtrType(new PredictionFactorType);
  ego_motion                                  = predictRobotMotion(control_input);
  robot_motion                                = srrg2_core::geometry2d::v2t(ego_motion);
  robot_motion_prediction                     = initial_pose_transform * robot_motion;
  StatePtrType pose_1                         = StatePtrType(new StateType);
  pose_1->setEstimate(Isometry2f::Identity());
  pose_1->setGraphId(2);
  // bb set time step of the predictor
  prediction_factor_0->setTimeStep(time_step);
  // bb variables ids in the prediction factor
  // bb using pose_0. pose_1 and control_0
  prediction_factor_0->setVariableId(0, 0);
  prediction_factor_0->setVariableId(1, 1);
  prediction_factor_0->setVariableId(2, 2);
  PriorFactorPtrType prior_factor_0 = PriorFactorPtrType(new PriorFactorType);
  prior_factor_0->setVariableId(0, 0);
  prior_factor_0->setMeasurement(initial_pose_transform);
  PriorFactorPtrType prior_factor_1 = PriorFactorPtrType(new PriorFactorType);
  prior_factor_1->setVariableId(0, 2);
  prior_factor_1->setMeasurement(robot_motion_prediction);

  FactorGraphPtr graph(new FactorGraph);
  graph->addFactor(prediction_factor_0);
  graph->addFactor(prior_factor_0);
  graph->addFactor(prior_factor_1);
  graph->addVariable(VariableBasePtr(pose_0));
  graph->addVariable(VariableBasePtr(pose_1));
  graph->addVariable(VariableBasePtr(control_0));
  graph->bindFactors();

  Solver solver;
  solver.param_max_iterations.pushBack(n_iterations);
  solver.param_termination_criteria.setValue(nullptr);
  solver.setGraph(graph);

  LOG << "Initial estimate control 0 \n " << control_0->estimate() << std::endl;
  LOG << "Initial estimate pose 0 \n " << pose_0->estimate().matrix() << std::endl;
  LOG << "Initial estimate pose 1 \n " << pose_1->estimate().matrix() << std::endl;

  solver.compute();

  const auto& stats                  = solver.iterationStats();
  const auto& estimated_pose_0       = pose_0->estimate();
  const auto& estimated_pose_1       = pose_1->estimate();
  const auto& normalized_chi_squared = stats.back().chi_normalized;

  LOG << "Final estimate control 0 \n " << control_0->estimate() << std::endl;
  LOG << "Final estimate pose 0 \n " << estimated_pose_0.matrix() << std::endl;
  LOG << "Final estimate pose 1 \n " << estimated_pose_1.matrix() << std::endl;
  LOG << "chi_normalized last iteration: " << normalized_chi_squared << std::endl;
}

