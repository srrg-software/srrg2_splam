#pragma once
#include <srrg_geometry/ad.h>
#include <srrg_geometry/geometry_defs.h>
#include "variable_control2_ad.h"

namespace srrg2_solver_extras {
  using srrg2_core::DualValuef;
  using srrg2_core::Vector3_;
  class DifferentialDriveControlPredictorAD {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    // bb using default constructor

    void setTimeStep(const float& time_step_) {
    	_time_step_ad = DualValuef(time_step_);
    }

  protected:
    Vector3_<DualValuef>
    _predictRobotMotion(const VariableControl2AD::ADEstimateType& control_input_) {
      const DualValuef linear_velocity      = control_input_.x();
      const DualValuef angular_velocity     = control_input_.y();
      const DualValuef angular_displacement = angular_velocity * _time_step_ad / 2;
      const DualValuef dx = linear_velocity * _time_step_ad * cos(angular_displacement);
      const DualValuef dy = linear_velocity * _time_step_ad * sin(angular_displacement);
      const DualValuef dt = angular_velocity * _time_step_ad;
      return Vector3_<DualValuef>(dx, dy, dt);
    }

    DualValuef _time_step_ad = DualValuef(0.0f);
  };

} // namespace srrg2_solver_extras
