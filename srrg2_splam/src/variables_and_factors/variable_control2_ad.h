#pragma once
#include <srrg_solver/variables_and_factors/types_2d/variable_point2_ad.h>

namespace srrg2_solver_extras {
  using VariableControl2AD = srrg2_solver::VariablePoint2AD;
} // namespace srrg2_solver_extras
