#pragma once
#include <srrg_solver/variables_and_factors/types_2d/instances.h>
#include "differential_drive_control_predictor_ad.h"
#include "variable_control2_ad.h"

namespace srrg2_solver_extras {
  using srrg2_core::DualValuef;
  using srrg2_core::Isometry2_;
  using srrg2_core::Isometry2f;
  using srrg2_solver::ADErrorFactor_;
  using srrg2_solver::MeasurementOwnerEigen_;
  using srrg2_solver::VariableSE2RightAD;
  class SE2MotionControlPredictionFactorAD
    : public ADErrorFactor_<3, VariableSE2RightAD, VariableControl2AD, VariableSE2RightAD>,
      public DifferentialDriveControlPredictorAD,
      public MeasurementOwnerEigen_<Isometry2f> {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    using BaseType = ADErrorFactor_<3, VariableSE2RightAD, VariableControl2AD, VariableSE2RightAD>;
    using BasePredictorType    = DifferentialDriveControlPredictorAD;
    using VariableTupleType    = typename BaseType::VariableTupleType;
    using ADErrorVectorType    = typename BaseType::ADErrorVectorType;
    using MeasurementOwnerType = MeasurementOwnerEigen_<Isometry2f>;
    using MeasurementTypeAD    = Isometry2_<DualValuef>;

    ADErrorVectorType operator()(VariableTupleType& vars) final {
      //! to retrieve a variable you should know the position in the parameter pack
      //! and the type

      const auto& prev_state    = vars.at<0>()->adEstimate();
      const auto& control_input = vars.at<1>()->adEstimate();
      const auto& current_state = vars.at<2>()->adEstimate();
      // do the computation (all in dual value)
      auto ego_motion                = _predictRobotMotion(control_input);
      MeasurementTypeAD robot_motion = srrg2_core::geometry2d::v2t(ego_motion);

      MeasurementTypeAD robot_motion_prediction = prev_state * robot_motion;

      return srrg2_core::geometry2d::t2v<DualValuef>(current_state.inverse() *
                                                     robot_motion_prediction);
    }
  };
} // namespace srrg2_solver_extras
