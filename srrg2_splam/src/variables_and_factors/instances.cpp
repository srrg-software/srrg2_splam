#include "instances.h"
#include "se2_motion_control_prediction_factor_ad.h"

namespace srrg2_solver_extras {
  void registerTypesControl() {
    BOSS_REGISTER_CLASS(SE2MotionControlPredictionFactorAD);
  }
} // namespace srrg2_solver_extras
